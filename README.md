# A "Prime" Example of Dos and Don'ts in Concurrency #

This provides some examples of where bugs can creep in when dealing with concurrency, and how to properly deal with them.

### Computing Primes ###

The purpose of the program is to determine which numbers are prime from a set of integers.  Integers are provided via message-passing from an ```IntegerGenerator```, to a ```PrimeComputer```.  The ```PrimeComputer``` determines whether or not a supplied integer is prime, and if so, stores it in a ```PrimeStorer```.

Various implementations of ```IntegerGenerator``` and ```PrimeStorer``` are provided to demonstrate the basic issues when dealing with concurrency.  Run the main program (```MainPrimeTests```) and go through the code to get a better understanding.