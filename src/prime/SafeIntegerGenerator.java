package prime;

/**
 * Safely generates sequential integers using the <code>synchronized</code> keyword
 *
 */
public class SafeIntegerGenerator implements IntegerGenerator {

	int next;
	int min;
	int max;
	
	public SafeIntegerGenerator(int min, int max) {
		this.min = min;
		this.next = min;
		this.max = max;
	}
	
	@Override
	/**
	 * Safely returns next integer.  The <code>synchronized</code> keyword ensures
	 * no two threads will simultaneously try to increment counter.
	 */
	public synchronized int getNext() {
		return next++;
	}
	
	/**
	 * Safely checks whether there is a next integer.  The <code>synchronized</code> 
	 * keyword ensures that no thread enters {@link #hasNext()} while another thread
	 * is simultaneously retrieving the next integer in {@link #getNext()}.  This 
	 * prevents the following race:
	 * <ul>
	 * <li>
	 * thread 1 enters {@link #getNext()}
	 * </li>
	 * <li>
	 * thread 2 enters {@link #hasNext()}
	 * </li>
	 * <li>
	 * thread 2 exits {@link #hasNext()}, returning that there is a remaining integer
	 * </li>
	 * <li>
	 * thread 1 grabs the last integer, so that {@link #hasNext()} will from now on
	 * return <code>false</code>
	 * </li>
	 * </ul>
	 * 
	 */
	public synchronized boolean hasNext() {
		if (next <= max) {
			return true;
		}
		return false;
	}
	
	public synchronized void reset() {
		next = min;
	}

}
