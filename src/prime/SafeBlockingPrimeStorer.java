package prime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Uses a synchronized (blocking) list safely
 */
public class SafeBlockingPrimeStorer implements PrimeStorer {

	List<Integer> storage = Collections.synchronizedList(new ArrayList<Integer>());
	
	@Override
	public void addPrime(int prime) {
		storage.add(prime);
	}

	@Override
	public int getNumPrimes() {
		return storage.size();
	}

	@Override
	public List<Integer> getPrimes() {
		return storage;
	}

	@Override
	public void clear() {
		storage.clear();
	}
	
}
