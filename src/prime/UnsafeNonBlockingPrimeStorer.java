package prime;

import java.util.ArrayList;
import java.util.List;

/**
 * Basic example of unsafe array access
 *
 */
public class UnsafeNonBlockingPrimeStorer implements PrimeStorer {

	List<Integer> storage = new ArrayList<Integer>();
	
	@Override
	public void addPrime(int prime) {
		storage.add(prime); // will lead to all kinds of exceptions
	}

	@Override
	public int getNumPrimes() {
		return storage.size();
	}

	@Override
	public List<Integer> getPrimes() {
		return storage;
	}

	@Override
	public void clear() {
		storage.clear();
	}
	
}
