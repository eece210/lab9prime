package prime;
/**
 * Example showing need to use caution, even when using the 
 * <code>synchronized</code> keyword
 *
 */
public class UnsafeIntegerGenerator2 implements IntegerGenerator {

	int next;
	int max;
	int min;
	
	public UnsafeIntegerGenerator2(int min, int max) {
		this.min = min;
		this.next = min;
		this.max = max;
	}
	
	@Override
	public synchronized int getNext() {
		if (next > max) {
			throw new RuntimeException("No next integer!!!");
		}
		return next++;
	}
	
	/**
	 * Unsafely checks whether there is a next integer.
	 * If another thread is currently grabbing the next
	 * integer using {@link #getNext()}, then this may
	 * incorrectly report that there are still
	 * integers available.
	 */
	public boolean hasNext() {
		if (next <= max) {
			return true;
		}
		return false;
	}
	
	@Override
	public void reset() {
		next = min;
	}

}
