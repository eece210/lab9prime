package prime;

import java.util.List;

/**
 * Storage for primes found
 * @author Antonio
 *
 */
public interface PrimeStorer {

	/**
	 * Add prime to storage
	 */
	public void addPrime(int prime);
	
	/**
	 * Returns the number of primes
	 */
	public int getNumPrimes();
	
	/**
	 * Returns the list of primes found
	 */
	public List<Integer> getPrimes();

	/**
	 * Clears storage, removing all primes
	 */
	public void clear();
	
	
}
