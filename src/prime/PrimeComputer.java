package prime;

/**
 * Computes whether integers are prime
 * @author Antonio
 *
 */
public class PrimeComputer implements Runnable {

	IntegerGenerator generator;
	PrimeStorer primes;
	
	/**
	 * Initializes computer
	 * @param generator generates integers to compute primes
	 * @param primes stores found primes
	 */
	public PrimeComputer(IntegerGenerator generator, PrimeStorer primes) {
		this.generator = generator;
		this.primes = primes;
	}
	
	@Override
	public void run() {
		
		while(generator.hasNext()) {
			// grab next integer
			int val = generator.getNext();
			// if prime, store
			if (isPrime(val)) {
				primes.addPrime(val);
			}
		}
		
	}
	
	/**
	 * Dumbly computes whether or not the supplied number is prime
	 */
	public boolean isPrime(int val) {
		
		// loop from 2 to val/2 (since can't be divible by v > val/2)
		for (int i=2; i <= val/2; i++) {
			if (val % i == 0) {
				return false;
			}
		}
		return true;
		
	}

}
