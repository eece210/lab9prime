package prime;

/**
 * Interface to generate integers.
 * @author Antonio
 *
 */
public interface IntegerGenerator {
	
	/**
	 * Retrieves the next generated integer if there is one (@see {@link #hasNext()}
	 */
	public int getNext();
	
	/**
	 * Determines if there is a new integer to grab
	 */
	public boolean hasNext();
	
	/**
	 * Re-initializes, so that {@link #getNext()} will reproducibly return the 
	 * original set of integers.  REPRODUCIBILITY IS REQUIRED!!
	 */
	public void reset();

}
