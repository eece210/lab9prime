package prime;

/**
 * 
 * Basic example of race conditions making this unsafe for multithreaded use
 *
 */
public class UnsafeIntegerGenerator implements IntegerGenerator {

	int next;
	int min;
	int max;
	
	public UnsafeIntegerGenerator(int min, int max) {
		this.min = min;
		this.next = min;
		this.max = max;
	}
	
	@Override
	public int getNext() {
		return next++;
	}
	
	public boolean hasNext() {
		if (next <= max) {
			return true;
		}
		return false;
	}

	@Override
	public void reset() {
		next = min;
	}
	
	

}
