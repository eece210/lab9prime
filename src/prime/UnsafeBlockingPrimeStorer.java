package prime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Example of when using a synchronized (blocking) list along may not solve issues
 *
 */
public class UnsafeBlockingPrimeStorer implements PrimeStorer {

	List<Integer> storage = Collections.synchronizedList(new ArrayList<Integer>());
	int numPrimes = 0;
	
	@Override
	public void addPrime(int prime) {
		storage.add(prime);
		numPrimes++;  // this makes the method unsafe!!
	}

	@Override
	public int getNumPrimes() {
		return numPrimes;
	}

	@Override
	public List<Integer> getPrimes() {
		return storage;
	}

	@Override
	public void clear() {
		storage.clear();
		numPrimes = 0;
	}
	
}
