package prime;

import java.util.List;


public class MainPrimeTests {
	
	/**
	 * Finds the set of primes produced by the supplied generator.
	 * 
	 * @param generator integer generator
	 * @param storer storage for storing produced primes
	 * @param numThreads number of threads to run
	 * @return number of primes found
	 */
	public static int findPrimes(IntegerGenerator generator, PrimeStorer storer, int numThreads) {
		
		generator.reset();
		storer.clear();
		
		// time function
		long startTime = System.nanoTime();
		
		// threads for computing primes
		Thread[] computers = new Thread[numThreads];
		for (int i=0; i<computers.length; i++) {
			computers[i] = new Thread(new PrimeComputer(generator, storer));
			computers[i].start();
		}
		
		// wait until complete
		for (Thread thread : computers) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		// end timing of function
		long endTime = System.nanoTime();
		long time = endTime-startTime;
		double sec = (double)time/1e9;
		
		// print results
		System.out.print("  Found " + storer.getNumPrimes() + " primes in " + String.format("%.2g",sec) + " seconds");
		// if few enough primes, print out the array
		if (storer.getNumPrimes() <= 100) {
			List<Integer> primes = storer.getPrimes();
			System.out.print("  [ ");
			int idx=0;
			for (Integer i : primes) {
				if (idx > 0) {
					System.out.print(", ");
				}
				System.out.print(i + " ");
			}
			System.out.print("]");			
		}
		System.out.println();
		
		
		return storer.getNumPrimes();
		
	}

	/**
	 * Does a bunch of prime tests
	 * @param args
	 */
	public static void main(String[] args) {
		
		int min = 1;
		int max = 50000;
		
		SafeIntegerGenerator safeIG = new SafeIntegerGenerator(min, max);
		UnsafeIntegerGenerator unsafeIG = new UnsafeIntegerGenerator(min, max);
		UnsafeIntegerGenerator2 unsafeIG2 = new UnsafeIntegerGenerator2(min, max);
		UnsafeIntegerGenerator3 unsafeIG3 = new UnsafeIntegerGenerator3(min, max);
		
		SafeBlockingPrimeStorer safeBP = new SafeBlockingPrimeStorer();
		SafeNonBlockingPrimeStorer safeNBP = new SafeNonBlockingPrimeStorer();
		UnsafeBlockingPrimeStorer unsafeBP = new UnsafeBlockingPrimeStorer();
		UnsafeNonBlockingPrimeStorer unsafeNBP = new UnsafeNonBlockingPrimeStorer();
		
		
		System.out.println("Single thread: ");
		findPrimes(safeIG, safeBP, 1);
		
		System.out.println("Ten threads, safe: ");
		findPrimes(safeIG, safeBP, 10);
		findPrimes(safeIG, safeNBP, 10);
		
		System.out.println("Ten threads, unsafe generator: ");
		findPrimes(unsafeIG, safeBP, 10);
		findPrimes(unsafeIG2, safeBP, 10);  // may need to run this many times to trigger error
		findPrimes(unsafeIG3, safeBP, 10);
		
		System.out.println("Ten threads, unsafe storer: ");
		findPrimes(safeIG, unsafeBP, 10);
		findPrimes(safeIG, unsafeNBP, 10);
		
	}
	
}
