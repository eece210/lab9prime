package prime;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Uses the <code>synchronized</code> keyword to ensure thread safety
 *
 */
public class SafeNonBlockingPrimeStorer implements PrimeStorer {

	List<Integer> storage = new ArrayList<Integer>();
	
	@Override
	public synchronized void addPrime(int prime) {
		storage.add(prime);
	}

	@Override
	public int getNumPrimes() {
		return storage.size();
	}

	@Override
	public List<Integer> getPrimes() {
		return storage;
	}

	@Override
	public synchronized void clear() {
		storage.clear();
	}
	
}
