package prime;

/**
 * Example showing unsafe use even with the <code>volatile</code> keyword
 * @author Antonio
 *
 */
public class UnsafeIntegerGenerator3 implements IntegerGenerator {

	volatile int next;  // volatile is only safe if writing doesn't depend on a previous read
	int max;
	int min;
	
	public UnsafeIntegerGenerator3(int min, int max) {
		this.min = min;
		this.next = min;
		this.max = max;
	}
	
	@Override
	public int getNext() {
		return next++;  // write depends on previous read, so is unsafe
	}
	
	public boolean hasNext() {
		if (next <= max) {
			return true;
		}
		return false;
	}
	
	@Override
	public void reset() {
		next = min;
	}

}
